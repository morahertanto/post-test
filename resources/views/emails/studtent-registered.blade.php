@component('mail::message')
# Congratulations

You are registered as student now.

@component('mail::button', ['url' => ''])
CONFIRM
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
