# Setup Proyek
- Clone Repositorynya
- Jalankan `composer install`
- Copy file `.env.example` ke `.env`
- Atur konfigurasi databasenya
- Jalankan `npm install` dan `npm run dev`
- Jalankan `php artisan migrate`
- Akses aplikasinya

# Misi
- Tulis test untuk file `StudentController`